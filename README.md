# meterweb

#### 介绍
MeterWeb是基于jmeter源码开发的b/s架构的自动化测试工具，支持分布式部署，实现多机并行测试；在线管理Slave节点，灵活增减测试节点；团队间也可以方便的共享测试用例。测试数据存储于mongodb中，可随时查看历史测试数据，生成测试报告；

#### 软件模块

- meterweb-cluster：slave节点运行程序
- meterweb-core：核心包
- meterweb-jmeter：jmeter源码
- meterweb-test: jmeter测试用例
- meterweb-web：Master控制台


#### 项目演示地址：
[https://meterweb.fetosoft.cn/login](https://meterweb.fetosoft.cn/login "https://meterweb.fetosoft.cn/login") ，默认用户名/密码：admin/000000；

#### 使用帮助
[http://fetosoft.cn/archives/2021/08/21/339](http://fetosoft.cn/archives/2021/08/21/339)

#### 完整安装包
链接:https://pan.baidu.com/s/1dx15nc8yZUUOhi-_1EsYWg 提取码:h1pt

#### 安装说明
[http://fetosoft.cn/archives/2021/08/22/355](http://fetosoft.cn/archives/2021/08/22/355)

#### 运行效果
![输入图片说明](https://images.gitee.com/uploads/images/2021/0925/170105_c7c92140_113373.png "meterweb.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0925/170124_f24ffab5_113373.png "clusters.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0925/170134_a52945de_113373.png "sampler.png")