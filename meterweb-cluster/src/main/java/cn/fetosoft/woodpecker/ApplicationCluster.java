package cn.fetosoft.woodpecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 12:37
 */
@SpringBootApplication(scanBasePackages = {"cn.fetosoft.woodpecker"})
public class ApplicationCluster {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationCluster.class, args);
        System.out.println("The cluster is start!!!\n");
    }
}
