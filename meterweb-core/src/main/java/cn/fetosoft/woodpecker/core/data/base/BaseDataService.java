package cn.fetosoft.woodpecker.core.data.base;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import java.util.List;

/**
 * 文档的基础操作
 * @author guobingbing
 * @create 2020-02-27 10:32
 */
public interface BaseDataService<T extends BaseEntity, P extends BaseForm> {

	/**
	 * 通过ID查询文档
	 * @author guobingbing
	 * @date 2020/12/1 9:03
	 * @param id
	 * @param clazz
	 * @return T
	 */
	T findById(String id, Class<T> clazz);

	/**
	 * 分页查询
	 * @author guobingbing
	 * @date 2020/12/1 17:40
	 * @param form
	 * @param clazz
	 * @return java.util.List<T>
	 */
	List<T> selectListByForm(P form, Class<T> clazz) throws Exception;

	/**
	 * 查询总记录数
	 * @param form
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	long selectCountByForm(P form, Class<T> clazz) throws Exception;

	/**
	 * 新增文档
	 * @author guobingbing
	 * @date 2020/12/1 10:09
	 * @param t
	 * @return T
	 */
	T insert(T t) throws Exception;

	/**
	 * 更新文档信息
	 * @author guobingbing
	 * @date 2020/12/1 11:45
	 * @param t
	 * @return com.mongodb.client.result.UpdateResult
	 */
	UpdateResult update(T t) throws Exception;

	/**
	 * 通过ID删除文档
	 * @author guobingbing
	 * @date 2020/12/1 15:30
	 * @param id
	 * @param clazz
	 * @return com.mongodb.client.result.DeleteResult
	 */
	DeleteResult deleteById(String id, Class<T> clazz);

	/**
	 * 根据多个条件删除
	 * @author guobingbing
	 * @wechat t_gbinb
	 * @date 2021/8/7 11:34
	 * @param form
	 * @param clazz
	 * @return com.mongodb.client.result.DeleteResult
	 * @version 1.0
	 */
	DeleteResult deleteByForm(BaseForm form, Class<T> clazz) throws Exception;
}
