package cn.fetosoft.woodpecker.core.data.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 更新的字段
 * @author guobingbing
 * @create 2020/12/1 10:59
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface DataField {

	/**
	 * 映射的文档字段名称，默认为字段名称相同
	 * @return
	 */
	String fieldName() default "";

	/**
	 * 支持更新的字段
	 * @return
	 */
	boolean updatable() default true;

	/**
	 * 当查询列表时所返回的字段，可以按需返回字段
	 * 避免一次返回数据量过大，影响性能
	 * @return
	 */
	boolean listable() default true;

	/**
	 * 是否唯一字段
	 * @return
	 */
	boolean unique() default false;
}
