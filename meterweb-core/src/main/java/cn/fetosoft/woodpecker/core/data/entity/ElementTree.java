package cn.fetosoft.woodpecker.core.data.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/29 22:22
 */
@Setter
@Getter
public class ElementTree {

    private BaseElement element;

    private List<ElementTree> children = new ArrayList<>();

    public void addChildTree(ElementTree child){
        this.children.add(child);
    }
}
