package cn.fetosoft.woodpecker.core.data.entity.element;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 测试计划
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 9:25
 */
@Setter
@Getter
@Document("wp_element")
public class TestPlanElement extends BaseElement {

	/**
	 * 函数模式
	 */
	@DataField
	private String functionalMode;

	@DataField
	private String tearDownOnShutdown;

	/**
	 * 串行执行
	 */
	@DataField
	private String serializeThreadGroups;

	/**
	 * 用户定义的类路径
	 */
	@DataField
	private String userDefineClassPath;
}
