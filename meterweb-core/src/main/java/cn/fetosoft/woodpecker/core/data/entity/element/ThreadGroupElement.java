package cn.fetosoft.woodpecker.core.data.entity.element;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/17 11:58
 */
@Setter
@Getter
@Document("wp_element")
public class ThreadGroupElement extends BaseElement {

	@DataField
	private String onSampleError;

	@DataField
	private boolean continueForever;

	/**
	 * 循环次数
	 */
	@DataField
	private int loops;

	/**
	 * 循环次数
	 */
	@DataField
	private int numThreads;

	@DataField
	private int rampTime;

	@DataField
	private boolean scheduler;

	/**
	 * 持续时间
	 */
	@DataField
	private long duration;

	/**
	 * 延迟启动时间
	 */
	@DataField
	private long delay;

	@DataField
	private boolean sameUserOnNextIteration;
}
