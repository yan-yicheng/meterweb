package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * CSV数据文件设置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/22 9:03
 */
@Setter
@Getter
@Document("wp_element")
public class CsvDataSetElement extends BaseElement {

	@DataField
	private String filename;
	@DataField
	private String filePath;
	@DataField
	private String fileEncoding;
	@DataField
	private String variableNames;
	@DataField
	private String ignoreFirstLine;
	@DataField
	private String delimiter;
	@DataField
	private String quotedData;
	@DataField
	private String recycle = "true";
	@DataField
	private String shareMode;
	@DataField
	private String stopThread;
}
