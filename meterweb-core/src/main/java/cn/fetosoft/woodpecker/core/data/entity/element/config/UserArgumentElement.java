package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.MultiArguments;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.List;

/**
 * 用户定义的变量
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/15 21:22
 */
@Setter
@Getter
@Document("wp_element")
public class UserArgumentElement extends BaseElement implements MultiArguments {

    @DataField
    private String[] names;
    @DataField
    private String[] values;
    @DataField
    private String[] descriptions;

	@Override
	public List<String> getFields() {
		return Arrays.asList("names", "values", "descriptions");
	}
}
