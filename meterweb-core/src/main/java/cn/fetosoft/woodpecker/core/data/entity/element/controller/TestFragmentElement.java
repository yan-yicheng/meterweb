package cn.fetosoft.woodpecker.core.data.entity.element.controller;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 测试片段
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/23 13:36
 */
@Setter
@Getter
@Document("wp_element")
public class TestFragmentElement extends BaseElement {

	public TestFragmentElement(){
		this.setEnabled(false);
	}
}
