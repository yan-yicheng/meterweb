package cn.fetosoft.woodpecker.core.data.entity.element.listener;

import cn.fetosoft.woodpecker.core.data.entity.BaseCollector;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 16:45
 */
@Setter
@Getter
@Document("wp_element")
public class AggregateReportElement extends BaseCollector {
}
