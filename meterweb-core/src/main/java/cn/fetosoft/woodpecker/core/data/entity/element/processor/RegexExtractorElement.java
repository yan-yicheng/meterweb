package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 正则表达式提取器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/21 13:53
 */
@Setter
@Getter
@Document("wp_element")
public class RegexExtractorElement extends BaseElement {

	@DataField
	private String scope;
	@DataField
	private String scopeVariable;
	@DataField
	private String useField;
	@DataField
	private String refname;
	@DataField
	private String regex;
	@DataField
	private String template;
	@DataField
	private String matchNumber;
	@DataField
	private String defaultValue;
	@DataField
	private String defaultEmptyValue;
}
