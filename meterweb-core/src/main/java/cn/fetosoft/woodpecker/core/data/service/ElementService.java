package cn.fetosoft.woodpecker.core.data.service;

import cn.fetosoft.woodpecker.core.data.base.BaseDataService;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.TestPlanElement;
import cn.fetosoft.woodpecker.core.data.form.ElementForm;

import java.util.Map;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/11 9:03
 */
public interface ElementService<T extends BaseElement> extends BaseDataService<T, ElementForm> {

	/**
	 * 查找最大的排序号
	 * @param planId 项目号
	 * @param parentId 父ID
	 * @param clazz 类
	 * @return
	 */
	int findMaxSort(String planId, String parentId, Class<T> clazz);

	/**
	 * 查询所有测试计划
	 * @param clazz
	 * @return
	 */
	Map<String, T> findElements(Class<T> clazz);
}
