package cn.fetosoft.woodpecker.core.data.service;

import cn.fetosoft.woodpecker.core.data.base.BaseDataService;
import cn.fetosoft.woodpecker.core.data.entity.TestResult;
import cn.fetosoft.woodpecker.core.data.form.TestResultForm;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 13:47
 */
public interface TestResultService extends BaseDataService<TestResult, TestResultForm> {

}
