package cn.fetosoft.woodpecker.core.data.service;

import cn.fetosoft.woodpecker.core.data.base.BaseDataService;
import cn.fetosoft.woodpecker.core.data.entity.User;
import cn.fetosoft.woodpecker.core.data.form.UserForm;

/**
 * 用户服务
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/14 21:42
 */
public interface UserService extends BaseDataService<User, UserForm> {

    User findByUserId(String userId);

    User login(String username, String password);

    User findByUsername(String username);
}
