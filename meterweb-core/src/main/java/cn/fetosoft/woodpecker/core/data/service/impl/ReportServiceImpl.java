package cn.fetosoft.woodpecker.core.data.service.impl;

import cn.fetosoft.woodpecker.core.data.base.AbstractMongoService;
import cn.fetosoft.woodpecker.core.data.entity.Report;
import cn.fetosoft.woodpecker.core.data.entity.TestResult;
import cn.fetosoft.woodpecker.core.data.form.ReportForm;
import cn.fetosoft.woodpecker.core.data.form.TestResultForm;
import cn.fetosoft.woodpecker.core.data.service.ReportService;
import cn.fetosoft.woodpecker.core.data.service.TestResultService;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Persistent report data
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 16:13
 */
@Service
public class ReportServiceImpl extends AbstractMongoService<Report, ReportForm>
		implements ReportService {

	@Autowired
	private TestResultService testResultService;

	@Override
	public long deleteAggregateById(String id) throws Exception {
		long count = 0;
		Report report = this.findById(id, Report.class);
		if(report!=null){
			TestResultForm form = new TestResultForm();
			form.setTestId(report.getTestId());
			form.setElementId(report.getElementId());
			DeleteResult dr1 = testResultService.deleteByForm(form, TestResult.class);
			DeleteResult dr2 = this.deleteById(id, Report.class);
			count = dr1.getDeletedCount() + dr2.getDeletedCount();
		}
		return count;
	}
}
