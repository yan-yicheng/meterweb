package cn.fetosoft.woodpecker.core.data.service.impl;

import cn.fetosoft.woodpecker.core.data.base.AbstractMongoService;
import cn.fetosoft.woodpecker.core.data.entity.TestResult;
import cn.fetosoft.woodpecker.core.data.form.TestResultForm;
import cn.fetosoft.woodpecker.core.data.service.TestResultService;
import org.springframework.stereotype.Service;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 13:51
 */
@Service
public class TestResultServiceImpl extends AbstractMongoService<TestResult, TestResultForm>
		implements TestResultService {

}
