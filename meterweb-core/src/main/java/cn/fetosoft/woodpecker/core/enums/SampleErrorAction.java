package cn.fetosoft.woodpecker.core.enums;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/17 13:36
 */
public enum SampleErrorAction {

	/**
	 * 继续
	 */
	CONTINUE("continue"),

	/**
	 * 启动下一进程循环
	 */
	START_NEXT_LOOP("startnextloop"),

	/**
	 * 停止
	 */
	STOP_THREAD("stopthread"),

	STOP_TEST("stoptest"),

	STOP_TEST_NOW("stoptestnow");

	private String name;

	SampleErrorAction(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}
