package cn.fetosoft.woodpecker.core.enums;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 17:47
 */
public enum ScheduledStatus {

	/**
	 *
	 */
	Running(1, "运行中"),

	Stop(2, "停止"),

	Delete(3, "删除");

	ScheduledStatus(int value, String text){
		this.value = value;
		this.text = text;
	}

	private int value;
	private String text;

	public int getValue(){
		return this.value;
	}

	public String getText(){
		return this.text;
	}
}
