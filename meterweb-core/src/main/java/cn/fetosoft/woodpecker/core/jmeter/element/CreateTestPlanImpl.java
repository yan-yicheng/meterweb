package cn.fetosoft.woodpecker.core.jmeter.element;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.TestPlanElement;
import cn.fetosoft.woodpecker.core.jmeter.TestPlanExtend;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 创建测试计划
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/11 20:17
 */
@Component("testPlanImpl")
public class CreateTestPlanImpl extends AbstractElementBuild implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        TestPlanElement element = (TestPlanElement) be;
        TestPlanExtend testPlan = new TestPlanExtend(this.applicationContext);
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlanExtend.class.getName());
        testPlan.setName(element.getName());
        testPlan.setComment(element.getDest());
        testPlan.setEnabled(true);
        testPlan.setPlanId(element.getPlanId());
        testPlan.setUserId(element.getUserId());
        testPlan.setTestId(element.getTestId());
        if(StringUtils.isNotBlank(element.getFunctionalMode())){
            testPlan.setFunctionalMode(Boolean.parseBoolean(element.getFunctionalMode()));
        }
        if(StringUtils.isNotBlank(element.getTearDownOnShutdown())){
            testPlan.setTearDownOnShutdown(Boolean.parseBoolean(element.getTearDownOnShutdown()));
        }
        if(StringUtils.isNotBlank(element.getSerializeThreadGroups())) {
            testPlan.setSerialized(Boolean.parseBoolean(element.getSerializeThreadGroups()));
        }
        testPlan.setTestPlanClasspath(element.getUserDefineClassPath());
        return testPlan;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
