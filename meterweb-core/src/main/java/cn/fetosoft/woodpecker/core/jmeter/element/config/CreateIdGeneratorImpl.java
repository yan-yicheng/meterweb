package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.IdGeneratorElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import cn.fetosoft.woodpecker.core.jmeter.extension.IdGeneratorConfig;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建随机ID生成器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/22 21:06
 */
@Component("idGeneratorImpl")
public class CreateIdGeneratorImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        IdGeneratorElement element = (IdGeneratorElement) be;
        IdGeneratorConfig config = new IdGeneratorConfig();
        config.setMode(element.getMode());
        config.setParamNames(element.getParamNames());
        config.setLength(element.getLength());
        config.setPattern(element.getPattern());
        config.setPrefix(element.getPrefix());
        return config;
    }
}
