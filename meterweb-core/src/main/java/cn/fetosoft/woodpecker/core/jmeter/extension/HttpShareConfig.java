package cn.fetosoft.woodpecker.core.jmeter.extension;

import org.apache.jmeter.config.ConfigElement;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.testelement.AbstractTestElement;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jmeter.testelement.property.CollectionProperty;
import org.apache.jmeter.threads.JMeterVariables;
import java.io.Serializable;

/**
 * Http共享配置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/11 10:12
 */
public class HttpShareConfig extends AbstractTestElement implements ConfigElement, TestStateListener, Serializable {

	@Override
	public void addConfigElement(ConfigElement config) {
	}

	@Override
	public boolean expectsModification() {
		return false;
	}

	@Override
	public void testStarted() {
		JMeterVariables vars = this.getThreadContext().getVariables();
		CollectionProperty properties = new CollectionProperty();
		properties.addProperty(getProperty(HTTPSamplerBase.PROTOCOL));
		properties.addProperty(getProperty(HTTPSamplerBase.DOMAIN));
		properties.addProperty(getProperty(HTTPSamplerBase.PORT));
		properties.addProperty(getProperty(HTTPSamplerBase.PATH));
		properties.addProperty(getProperty(HTTPSamplerBase.CONTENT_ENCODING));
		properties.addProperty(getProperty(HTTPSamplerBase.CONNECT_TIMEOUT));
		properties.addProperty(getProperty(HTTPSamplerBase.RESPONSE_TIMEOUT));
		vars.putObject(HTTPSamplerBase.DEFAULT_CONFIG, properties);
	}

	@Override
	public void testStarted(String host) {
		this.testStarted();
	}

	@Override
	public void testEnded() {

	}

	@Override
	public void testEnded(String host) {

	}

	public String getProtocol() {
		return this.getPropertyAsString(HTTPSamplerBase.PROTOCOL);
	}

	public void setProtocol(String protocol) {
		this.setProperty(HTTPSamplerBase.PROTOCOL, protocol);
	}

	public String getDomain() {
		return this.getPropertyAsString(HTTPSamplerBase.DOMAIN);
	}

	public void setDomain(String domain) {
		this.setProperty(HTTPSamplerBase.DOMAIN, domain);
	}

	public String getPort() {
		return this.getPropertyAsString(HTTPSamplerBase.PORT);
	}

	public void setPort(String port) {
		this.setProperty(HTTPSamplerBase.PORT, port);
	}

	public String getPath() {
		return this.getPropertyAsString(HTTPSamplerBase.PATH);
	}

	public void setPath(String path) {
		this.setProperty(HTTPSamplerBase.PATH, path);
	}

	public String getContentEncode() {
		return this.getPropertyAsString(HTTPSamplerBase.CONTENT_ENCODING);
	}

	public void setContentEncode(String contentEncode) {
		this.setProperty(HTTPSamplerBase.CONTENT_ENCODING, contentEncode);
	}

	public String getConnectTimeout() {
		return this.getPropertyAsString(HTTPSamplerBase.CONNECT_TIMEOUT);
	}

	public void setConnectTimeout(String connectTimeout) {
		this.setProperty(HTTPSamplerBase.CONNECT_TIMEOUT, connectTimeout);
	}

	public String getResponseTimeout() {
		return this.getPropertyAsString(HTTPSamplerBase.RESPONSE_TIMEOUT);
	}

	public void setResponseTimeout(String responseTimeout) {
		this.setProperty(HTTPSamplerBase.RESPONSE_TIMEOUT, responseTimeout);
	}
}
