package cn.fetosoft.woodpecker.core.pubsub.zookeeper;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

/**
 * 节点数据
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/12 12:01
 */
@Setter
@Getter
public class NodeEntity {

    private String id;

    private String ip;

    private String path;

    private int cpuCore;

    private long totalMemory;

    private boolean enabled = true;

    private long regTime;

    @Override
    public String toString(){
        return JSON.toJSONString(this);
    }
}
