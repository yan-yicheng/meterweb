package cn.fetosoft.woodpecker.core.scheduled;

import cn.fetosoft.woodpecker.core.jmeter.JMeterService;
import cn.fetosoft.woodpecker.core.util.RandomUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 启动执行计划
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/28 9:16
 */
@DisallowConcurrentExecution
public class TestPlanExecuteJob implements Job {

	@Autowired
	private JMeterService jMeterService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String planId = dataMap.getString("planId");
		String userId = dataMap.getString("userId");
		try {
			jMeterService.startTest(userId, RandomUtil.uuid(), planId);
			System.out.println("The plan started that planId is " + planId);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
}
