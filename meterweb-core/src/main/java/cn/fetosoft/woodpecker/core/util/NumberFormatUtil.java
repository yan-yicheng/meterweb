package cn.fetosoft.woodpecker.core.util;

import java.text.NumberFormat;

/**
 * 数字格式化
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/11 10:23
 */
public class NumberFormatUtil {

    /**
     * 格式化小数
     * @param num
     * @param digits
     * @return
     */
    public static String doubleFormat(double num, int digits){
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(digits);
        return nf.format(num);
    }
}
