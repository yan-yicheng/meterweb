package org.apache.mytest;

import org.apache.jmeter.JMeter;
import org.apache.jmeter.exceptions.IllegalUserActionException;
import org.apache.jmeter.junit.JMeterTestCase;
import org.apache.jmeter.report.config.ConfigurationException;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;
import org.apache.jorphan.test.JMeterSerialTest;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/2 10:36
 */
public class HashTreeTest extends JMeterTestCase implements JMeterSerialTest {

	@Test
	public void buildTree(){
		JMeterUtils.setJMeterHome("D:\\git\\jmeter-dev");
		java.net.URL uri = this.getClass().getResource("/");
		System.out.println(uri.getPath());
		File file = new File(uri.getPath() + "/线程组.jmx");
		try {
			HashTree tree = SaveService.loadTree(file);
			System.out.println(tree.size());


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void startNonGui(){
		JMeterUtils.setJMeterHome("D:\\git\\jmeter-dev");
		java.net.URL uri = this.getClass().getResource("/");
		System.out.println(uri.getPath());
		JMeter jmeter = new JMeter();
		try {
			//jmeter.startNonGui(uri.getPath() + "/线程组.jmx", null, null, false);
			File temp = File.createTempFile("testPlan", ".jmx");
			String testPlan = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
					+ "<jmeterTestPlan version=\"1.2\" properties=\"5.0\" jmeter=\"5.2-SNAPSHOT\">\n" + "  <hashTree>\n"
					+ "    <TestPlan guiclass=\"TestPlanGui\" testclass=\"TestPlan\" testname=\"Test Plan\" enabled=\"true\">\n"
					+ "      <stringProp name=\"TestPlan.comments\"></stringProp>\n"
					+ "      <boolProp name=\"TestPlan.functional_mode\">false</boolProp>\n"
					+ "      <boolProp name=\"TestPlan.tearDown_on_shutdown\">true</boolProp>\n"
					+ "      <boolProp name=\"TestPlan.serialize_threadgroups\">false</boolProp>\n"
					+ "      <elementProp name=\"TestPlan.user_defined_variables\" elementType=\"Arguments\" guiclass=\"ArgumentsPanel\" "
					+ "testclass=\"Arguments\" testname=\"User Defined Variables\" enabled=\"true\">\n"
					+ "        <collectionProp name=\"Arguments.arguments\"/>\n" + "      </elementProp>\n"
					+ "      <stringProp name=\"TestPlan.user_define_classpath\"></stringProp></TestPlan>"
					+ "    <hashTree/></hashTree></jmeterTestPlan>";
			try (FileWriter fw = new FileWriter(temp);
				 BufferedWriter out = new BufferedWriter(fw)) {
				out.write(testPlan);
			}
			jmeter.startNonGui(temp.getAbsolutePath(), null, null, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sendSmsNonGUI(){
		JMeterUtils.setJMeterHome("D:\\git\\jmeter-dev");
		java.net.URL uri = this.getClass().getResource("/");
		System.out.println(uri.getPath());
		JMeter jmeter = new JMeter();
		try{
			jmeter.startNonGui(uri.getPath() + "sendSms.xml", null, null, false);
			TimeUnit.SECONDS.sleep(5);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
