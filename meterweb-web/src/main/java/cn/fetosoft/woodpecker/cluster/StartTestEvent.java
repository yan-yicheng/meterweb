package cn.fetosoft.woodpecker.cluster;

import org.springframework.context.ApplicationEvent;

/**
 * 启动测试事件
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/20 17:30
 */
public class StartTestEvent extends ApplicationEvent {

    private String userId;
    private String testId;
    private String planId;

    /**
     *
     */
    public StartTestEvent(String userId, String testId, String planId) {
        super(testId);
        this.userId = userId;
        this.testId = testId;
        this.planId = planId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }
}
