package cn.fetosoft.woodpecker.cluster;

import cn.fetosoft.woodpecker.core.pubsub.Command;
import cn.fetosoft.woodpecker.core.pubsub.Publication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/20 17:37
 */
@Component
public class StartTestListener implements ApplicationListener<StartTestEvent> {

    private static final Logger logger = LoggerFactory.getLogger(StartTestListener.class);
    @Autowired
    private Publication publication;

    @Override
    public void onApplicationEvent(StartTestEvent event) {
        Command cmd = new Command();
        cmd.setCommand(Command.CMD_START_TEST);
        cmd.setUserId(event.getUserId());
        cmd.setTestId(event.getTestId());
        cmd.setPlanId(event.getPlanId());
        try {
            publication.publish(cmd.toString());
        } catch (Exception e) {
            logger.error("startTestListener", e);
        }
    }
}
