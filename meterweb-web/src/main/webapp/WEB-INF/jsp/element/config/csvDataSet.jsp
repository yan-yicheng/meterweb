<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">CSV数据文件设置</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','csvDataSet_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#csvDataSet_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="csvDataSet_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId" id="csvDataSet_form_planId_${eid}">
                    <input type="hidden" name="category">
                    <input type="hidden" name="filePath" id="csvDataSet_form_filePath_${eid}">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="csvDataSet_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:160,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:160">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="filename" id="csvDataSet_form_filename_${eid}" style="width:90%"
                               data-options="label:'文件名称：',labelWidth:160,readonly:true,required:true">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-filebox" id="csvDataSet_form_file_${eid}" style="width:90%"
                               data-options="prompt:'Choose a file...',label:'CSV文件上传：',buttonAlign:'right',buttonText:'浏览文件',labelWidth:160,onChange:function(newValue,oldValue){
                                let planId = $('#csvDataSet_form_planId_${eid}').val();
                                wpElement.uploadCsvFile('${eid}',planId);
                               }">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="fileEncoding" style="width:90%" data-options="label:'文件编码：',labelWidth:160">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="variableNames" style="width:90%" data-options="label:'变量名称：',labelWidth:160">
                        <p style="padding: 3px 0 0 160px;">多个变量间用英文“,”分隔</p>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="ignoreFirstLine" value="true" data-options="label:'忽略首行：',labelWidth:160">&nbsp;&nbsp;只在设置了变量名称后才生效
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="delimiter" style="width:90%" data-options="label:'字段分隔符：',labelWidth:160">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="quotedData" value="true" data-options="label:'是否允许带引号：',labelWidth:160">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="recycle" value="true" data-options="label:'文件结束符后再循环：',labelWidth:160">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-checkbox" name="stopThread" value="true" data-options="label:'文件结束符后停止线程：',labelWidth:160">
                    </div>
                    <div>
                        <select class="easyui-combobox" name="shareMode" style="width:50%;" data-options="label:'线程共享模式：',panelMaxHeight:150,labelWidth:160">
                            <option value="shareMode.all">所有线程组</option>
                            <option value="shareMode.group">当前线程组</option>
                            <option value="shareMode.thread">当前线程</option>
                        </select>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','csvDataSet_form_',function(eid,status) {
                    if(status===1){
                    menuTree.updateNodeText($('#csvDataSet_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>