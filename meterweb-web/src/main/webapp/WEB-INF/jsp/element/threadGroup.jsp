<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">线程组设置</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','threadGroup_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#threadGroup_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="threadGroup_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="threadGroup_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-3">取样器异常后要执行的动作：</div>
                            <div class="col-9">
                                <input class="easyui-radiobutton" name="onSampleError" value="continue"
                                       data-options="label:'继续',labelPosition:'after',labelWidth:80,checked:true">
                                <input class="easyui-radiobutton" name="onSampleError" value="startnextloop"
                                       data-options="label:'启动下一进程循环',labelPosition:'after',labelWidth:140">
                                <input class="easyui-radiobutton" name="onSampleError" value="stopthread"
                                       data-options="label:'停止线程',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="onSampleError" value="stoptest"
                                       data-options="label:'停止测试',labelPosition:'after',labelWidth:100">
                                <input class="easyui-radiobutton" name="onSampleError" value="stoptestnow"
                                       data-options="label:'立即停止测试',labelPosition:'after',labelWidth:100">
                            </div>
                        </div>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-numberbox" name="numThreads" style="width:50%" data-options="label:'线程数：',labelWidth:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-numberbox" name="rampTime" style="width:50%" data-options="label:'RampUp时间（秒）：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-numberbox" name="loops" style="width:50%" data-options="label:'循环次数：',labelWidth:150,required:true">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>
                        <input class="easyui-checkbox" name="scheduler" value="true"
                               data-options="label:'调度器：',labelWidth:150,
                                                        onChange:function(checked){
                                                            schedulerHandle(checked);
                                                        }">
                    </p>
                    <div class="form_item_height">
                        <input id="threadGroup_form_duration_${eid}" class="easyui-numberbox" name="duration" style="width:50%"
                               data-options="label:'持续时间（秒）：',labelWidth:150,disabled:true">
                    </div>
                    <div class="form_item_height">
                        <input id="threadGroup_form_delay_${eid}" class="easyui-numberbox" name="delay" style="width:50%"
                               data-options="label:'自动延迟（秒）：',labelWidth:150,disabled:true">
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','threadGroup_form_',function(eid,status) {
                    if(status===1){
                    menuTree.updateNodeText($('#threadGroup_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    function schedulerHandle(checked) {
        if(checked){
            $('#threadGroup_form_duration_${eid}').textbox({required:true,disabled:false});
            $('#threadGroup_form_delay_${eid}').textbox({required:true,disabled:false});
        }else{
            $('#threadGroup_form_duration_${eid}').textbox({required:false,disabled:true});
            $('#threadGroup_form_delay_${eid}').textbox({required:false,disabled:true});
        }
    }
</script>
