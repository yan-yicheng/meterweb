<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="easyui-layout" fit="true" style="margin: 0px;border: 0px;overflow: hidden;width:100%;height:100%;">
    <div data-options="region:'north',border:false" style="overflow: hidden;margin: 5px;" >
        <form id="qm_aggregate">
            <div style="margin: 5px 0">
                <input class="easyui-combobox" name="planId" data-options="
                    url: '/auth/element/getPlansDict',
                    method:'get',
                    editable: false,
                    valueField: 'id',
                    textField: 'text',
                    panelMaxHeight: 180,
                    label: '测试计划：',
                    width:300
                ">
                <input class="easyui-datebox" name="testDate" data-options="label:'测试日期：',labelAlign:'right',width:230">
                <input class="easyui-textbox" name="testId" data-options="label:'测试批次：',labelAlign:'right',width:380">
            </div>
        </form>
    </div>
    <div data-options="region:'center',split:false, border:false" style="padding:0; overflow-y: hidden;">
        <table id="dg_aggregate" class="easyui-datagrid"
               data-options="rownumbers:true,singleSelect:false,pagination:true,
               url:'${ctx}/auth/report/getAggregateList',
               method:'post',pageSize:20,fit:true,toolbar:'#aggregateTopBtn',border:true">
            <thead>
                <tr>
                    <th data-options="field:'id',checkbox:true"></th>
                    <th data-options="field:'createTimeFmt',width:200">测试时间</th>
                    <th data-options="field:'testId',width:280">测试批次</th>
                    <th data-options="field:'name',width:180">名称</th>
                    <th data-options="field:'samples',width:100,align:'right'">样本数</th>
                    <th data-options="field:'average',width:120,align:'right'">平均响应时间ms</th>
                    <th data-options="field:'median',width:120,align:'right'">50%响应时间</th>
                    <th data-options="field:'percent90',width:120,align:'right'">90%响应时间</th>
                    <th data-options="field:'percent95',width:120,align:'right'">95%响应时间</th>
                    <th data-options="field:'percent99',width:120,align:'right'">99%响应时间</th>
                    <th data-options="field:'min',width:120,align:'right'">最小响应时间ms</th>
                    <th data-options="field:'max',width:120,align:'right'">最大响应时间ms</th>
                    <th data-options="field:'errorRate',width:100,align:'right'">异常%</th>
                    <th data-options="field:'throughputFmt',width:100,align:'right'">吞吐量</th>
                    <th data-options="field:'sendBytesFmt',width:100,align:'right'">接收KB/s</th>
                    <th data-options="field:'receiveBytesFmt',width:100,align:'right'">发送KB/s</th>
                </tr>
            </thead>
        </table>
        <div id="aggregateTopBtn" style="padding:2px 5px;">
            <a href="javascript:systemLib.searchAggregate()" class="btn btn-ghost-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M12.442 12.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M8.5 14a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM15 8.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"></path>
                </svg>
                查询
            </a>
            <a href="javascript:$('#qm_aggregate').form('reset')" class="btn btn-ghost-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M10 17a7 7 0 100-14 7 7 0 000 14zm0 1a8 8 0 100-16 8 8 0 000 16z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M12.646 13.354l-6-6 .708-.708 6 6-.708.708z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M7.354 13.354l6-6-.708-.708-6 6 .708.708z" clip-rule="evenodd"></path>
                </svg>
                清空
            </a>
            <a href="javascript:systemLib.deleteAggregate()" class="btn btn-ghost-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.5 7.5A.5.5 0 018 8v6a.5.5 0 01-1 0V8a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V8a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V8z"></path>
                    <path fill-rule="evenodd" d="M16.5 5a1 1 0 01-1 1H15v9a2 2 0 01-2 2H7a2 2 0 01-2-2V6h-.5a1 1 0 01-1-1V4a1 1 0 011-1H8a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM6.118 6L6 6.059V15a1 1 0 001 1h6a1 1 0 001-1V6.059L13.882 6H6.118zM4.5 5V4h11v1h-11z" clip-rule="evenodd"></path>
                </svg>
                删除
            </a>
            <a href="javascript:$('#dg_aggregate').datagrid('reload')" class="btn btn-ghost-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4 9.5a.5.5 0 00-.5.5 6.5 6.5 0 0012.13 3.25.5.5 0 00-.866-.5A5.5 5.5 0 014.5 10a.5.5 0 00-.5-.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M4.354 9.146a.5.5 0 00-.708 0l-2 2a.5.5 0 00.708.708L4 10.207l1.646 1.647a.5.5 0 00.708-.708l-2-2zM15.947 10.5a.5.5 0 00.5-.5 6.5 6.5 0 00-12.13-3.25.5.5 0 10.866.5A5.5 5.5 0 0115.448 10a.5.5 0 00.5.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M18.354 8.146a.5.5 0 00-.708 0L16 9.793l-1.646-1.647a.5.5 0 00-.708.708l2 2a.5.5 0 00.708 0l2-2a.5.5 0 000-.708z" clip-rule="evenodd"></path>
                </svg>
                刷新
            </a>
        </div>
    </div>
</div>