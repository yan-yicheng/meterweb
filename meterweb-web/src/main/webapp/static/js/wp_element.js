/**
 * 各组件逻辑函数
 * @param rootPath
 * @constructor
 */
function WpElement(rootPath) {
    this.rootPath = rootPath;
}

WpElement.prototype.uploadFile = function (formData, uploadUrl, callbackFunc) {
    $.ajax({
        type: 'POST',
        url: uploadUrl,
        contentType: false,
        processData: false,
        mimeType: 'multipart/form-data',
        enctype: 'multipart/form-data',
        data: formData,
        dataType: "json",
        success: function (result) {
            if(result.status===1){
                callbackFunc(true, result.obj);
            }else{
                callbackFunc(false, result.errorMsg);
            }
        },
        error: function (result) {
            callbackFunc(false, result);
        }
    });
};

WpElement.prototype.uploadCsvFile = function (eid, planId) {
    let that = this;
    let fieldId = 'csvDataSet_form_file_' + eid;
    let file = $('#'+fieldId).filebox('files')[0];
    if(file){
        let name = file.name;
        let ext = name.substr(name.lastIndexOf('.')).toLowerCase();
        if(ext==='.csv'){
            let lay = layerLoading(1);
            let formData = new FormData();
            formData.append("file", file);
            formData.append("planId", planId);
            let uploadUrl = that.rootPath + '/auth/common/upload';
            that.uploadFile(formData, uploadUrl, function (isSuccess, data) {
                layer.close(lay);
                if(isSuccess){
                    $('#csvDataSet_form_filePath_' + eid).val(data.absolutePath);
                    $('#csvDataSet_form_filename_' + eid).textbox('setValue',data.name);
                }else{
                    layer.alert(data);
                }
            });
        }else{
            layer.alert('请上传csv格式的文件！');
        }
    }else{
        layer.alert('请选择文件！');
    }
};

WpElement.prototype.moduleTreeChecked = function (eid) {
    let that = this;
    $.get(that.rootPath + '/auth/element/getDetail?id=' + eid, function (data) {
        if(data.status===1){
            let fragments = data.obj.fragments;
            if(fragments){
                let frags = fragments.split(',');
                if(frags.length>0){
                    let otree = $('#moduleController_tree_' + eid);
                    $.each(frags, function (index, id) {
                        let node = otree.tree('find', function(node){
                            return node.id===id;
                        });
                        if(node){
                            otree.tree('check', node.target);
                        }
                    });
                }
            }
        }
    });
};

WpElement.prototype.saveModuleController = function (eid) {
    let nodes = $('#moduleController_tree_' + eid).tree('getChecked');
    let ofragments = $('#moduleController_form_fragments_' + eid);
    if(nodes.length>0){
        let arr = [];
        $.each(nodes, function (index, n) {
            arr.push(n.id);
        });
        ofragments.val(arr.join(','));
    }else{
        ofragments.val('');
    }
    testElement.saveElement(eid,'moduleController_form_',function(eid,status) {
        if(status===1){
            menuTree.updateNodeText($('#moduleController_form_name_' + eid).val());
        }
    })
};

WpElement.prototype.checkLoopControllerForever = function (checked, eid) {
    let loopBox = $('#loopController_form_loops_' + eid);
    if(checked){
        loopBox.textbox('setValue', '');
        loopBox.textbox({required:false,disabled:true});
    }else{
        loopBox.textbox({required:true,disabled:false});
    }
};

WpElement.prototype.checkJsonAssertionValidation = function (checked, eid) {
    let regexBox = $('#jsonPathAssertion_form_isRegex_' + eid);
    let valueBox = $('#jsonPathAssertion_form_expectedValue_' + eid);
    if(checked){
        regexBox.checkbox({disabled:false});
        valueBox.textbox({disabled:false});
    }else{
        regexBox.checkbox({disabled:true});
        valueBox.textbox({disabled:true});
    }
};

WpElement.prototype.userArgumentAdd = function (container, name, val, dest) {
    let parentRow = $("<div class='row pt-2'></div>");
    let nameField = $("<div class='col-4'><input type='text' class='form-control' name='names' value='"+name+"'></div>");
    nameField.appendTo(parentRow);
    let valueField = $("<div class='col-3'><input type='text' class='form-control' name='values' value='"+val+"'></div>");
    valueField.appendTo(parentRow);
    let destField = $("<div class='col-3'><input type='text' class='form-control' name='descriptions' value='"+dest+"'></div>");
    destField.appendTo(parentRow);
    let delField = $("<div class='col-2'></div>");
    let delButton = $("<a href='javascript:void(0)' class='btn btn-outline-danger'>删除</a>");
    delButton.appendTo(delField);
    delField.appendTo(parentRow);
    parentRow.appendTo(container);

    delButton.click({row:parentRow}, function (event) {
        event.data.row.remove();
    });
};

WpElement.prototype.httpSamplerChange = function(eid, contentType){
    let bodyDiv = $('#httpSampler_body_' + eid);
    let argumentDiv = $('#httpSampler_argument_' + eid);
    if(contentType==='json'){
        bodyDiv.css('display', 'block');
        argumentDiv.css('display', 'none');
    }else{
        bodyDiv.css('display', 'none');
        argumentDiv.css('display', 'block');
    }
};

WpElement.prototype.httpParameterAdd =  function (container, name, val) {
    let parentRow = $("<div class='row pt-2'></div>");
    let nameField = $("<div class='col-5'><input type='text' class='form-control' name='names' value='"+name+"'></div>");
    nameField.appendTo(parentRow);
    let valueField = $("<div class='col-5'><input type='text' class='form-control' name='values' value='"+val+"'></div>");
    valueField.appendTo(parentRow);
    let delField = $("<div class='col-2'></div>");
    let delButton = $("<a href='javascript:void(0)' class='btn btn-outline-danger'>删除</a>");
    delButton.appendTo(delField);
    delField.appendTo(parentRow);

    parentRow.appendTo(container);
    delButton.click({row:parentRow}, function (event) {
        event.data.row.remove();
    });
};

WpElement.prototype.getClusters = function () {
    let that = this;
    let container = $('#cluster_container');
    container.empty();
    $.get(that.rootPath + '/auth/cluster/getClusters', function (result) {
        if(result.status===1){
            $.each(result.obj, function (index, node) {
                let col = $("<div class='col-4'></div>");
                let card = $("<div class='card'></div>");
                let header = $("<div class='card-header'></div>");
                let title = $("<h3 class='card-title'>节点：" + node.ip + "</h3>");
                title.appendTo(header);
                header.appendTo(card);

                let body = $("<div class='card-body'></div>");
                let cpu = $("<p>CPU Core：" + node.cpuCore + "核</p>");
                cpu.appendTo(body);
                let memory = $("<p>JVM内存：" + node.totalMemory + "MB</p>");
                memory.appendTo(body);
                let checkbox = $("<input id='cluster_box_"+node.id+"' type='checkbox' style='width: 15px;height: 15px;' />");
                checkbox.prop('checked', node.enabled);
                checkbox.click({'id':node.id,'path':node.path}, function (event) {
                    that.clusterEnabled(event.data.id, event.target.checked);
                });
                let p = $("<p>是否启用：</p>");
                checkbox.appendTo(p);
                p.appendTo(body)
                body.appendTo(card);

                card.appendTo(col);
                col.appendTo(container);
            });
        }else{
            layer.alert(result.errorMsg);
        }
    },'json');
};

WpElement.prototype.clusterEnabled = function (clusterId, enabled) {
    let that = this;
    let data = {'clusterId':clusterId, 'enabled':enabled};
    $.post(that.rootPath + '/auth/cluster/enabled', data, function (result) {
        if(result.status===1){
            //that.getClusters();
        }else{
            layer.alert(result.errorMsg);
        }
    },'json');
};